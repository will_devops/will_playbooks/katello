Katello  
=========  
  
A ansible role for Katello + Foreman installation  
  
Requirements  
------------  
  
Ansible installed  
  
Default Variables  
--------------  
  
### General Configuration  
**admin_user:** admin  
**admin_pass:** P@ssw0rd  
**org:** example.com  
**location:** datacenter  
**foreman_version:** 1.16  
**katello_version:** 3.5  
  
### DHCP Configuration  
**dhcp:** "true"  
**dhcp_gateway:** 172.16.0.2  
**dhcp_interface:** eth1  
**dhcp_nameservers:** 172.16.0.2  
**dhcp_range:** 172.16.0.100 172.16.0.200  
**dhcp_server:** 172.16.0.2  
  
### DNS Configuration  
**dns:** "true"  
**dns_forwarders:** 192.168.200.1  
**dns_interface:** eth1  
**dns_reverse:** 0.16.172.in-addr.arpa  
**dns_server:** 172.16.0.2  
  
### Management Configuration  
**puppetrun:** "true"   
  
Dependencies  
------------  
  
None  
  
Example Playbook  
----------------  
  
    - hosts: servers  
      roles:  
         - { role: katello }  
  
License  
-------  
  
BSD  